package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.*;
import models.vo.ShipDistribution;
import play.libs.F;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Result;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author adewaleafolabi
 */
public class DashboardController extends Controller {

    public Promise<Result> index() {

        Map<String, Integer> stat = new HashMap<>();
        stat.put("ship", Ship.find.findRowCount());
        stat.put("location", Location.find.findRowCount());
        stat.put("product", Product.find.findRowCount());
        stat.put("user", User.find.where().ne("role", Role.SUPER_ADMIN).findRowCount());
        stat.put("inventory", Inventory.find.findRowCount());

        final String SHIP_PRODUCT_DISTRIBUTION_QUERY = "select count(p.ship_id) \"AMOUNT\",s.description \"SHIP\" from product p  left join ship s on p.ship_id= s.id group by p.ship_id having p.ship_id is not null order by SHIP ASC";

        RawSql rawSql = RawSqlBuilder.unparsed(SHIP_PRODUCT_DISTRIBUTION_QUERY).create();

        List<ShipDistribution> shipDistributionList = new ArrayList<>();

        Ebean.createSqlQuery(rawSql.getSql().getUnparsedSql()).findList().stream().forEach(sqlRow -> {
            shipDistributionList.add(new ShipDistribution(sqlRow.getString("SHIP"), sqlRow.getInteger("AMOUNT")));
        });

        return F.Promise.pure(ok(views.html.dashboard.index.render(stat, shipDistributionList)));
    }
}