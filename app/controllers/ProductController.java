package controllers;

import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Inventory;
import models.Location;
import models.Product;
import models.Ship;
import play.data.Form;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.Secured;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Map;
import java.util.Optional;

/**
 * @author adewaleafolabi
 */
@Security.Authenticated(Secured.class)
public class ProductController extends Controller {

    public F.Promise<Result> index() {

        return F.Promise.pure(ok(views.html.product.list.render()));
    }

    public F.Promise<Result> create() {
        return Ship.find().map(ships -> {
            Form<Product> productCreationForm = Form.form(Product.class);
            return ok(views.html.product.create.render(productCreationForm, ships));

        });
    }


    public F.Promise<Result> update() {

        final Form<Product> productForm = Form.form(Product.class).bindFromRequest();

        if (productForm.hasErrors()) {

            play.Logger.error("Form binding failed with errors -- {}", productForm.errorsAsJson().toString());

            return Ship.find().map(ships -> {
                Form<Product> productCreationForm = Form.form(Product.class);
                return badRequest(views.html.product.edit.render(productCreationForm, ships));

            });
        }

        Product product = productForm.get();

        return product.asyncUpdate().map(aVoid -> {
            flash("success", "Product information has been updated");

            return redirect(routes.ProductController.index());
        }).recover(throwable -> {

            String flashMessage = "";

            play.Logger.error("Problem in updatin product information {}", throwable);

            if (throwable.getCause() instanceof SQLIntegrityConstraintViolationException) {

                flashMessage = "The supplied product serial number is already in use";
            } else {

                flashMessage = "A technical error occurred. Please try again later";
            }

            flash("danger", flashMessage);


            return badRequest(views.html.product.edit.render(productForm, Ship.find.all()));

        });
    }

    public F.Promise<Result> save() {

        final Form<Product> productForm = Form.form(Product.class).bindFromRequest();

        if (productForm.hasErrors()) {

            play.Logger.error("Form binding failed with errors -- {}", productForm.errorsAsJson().toString());

            return Ship.find().map(ships -> {
                Form<Product> productCreationForm = Form.form(Product.class);
                return badRequest(views.html.product.create.render(productCreationForm, ships));

            });
        }

        Product product = productForm.get();

        return product.asyncSave().map(aVoid -> {
            flash("success", "Product information has been saved");

            return redirect(routes.ProductController.index());
        }).recover(throwable -> {

            String flashMessage = "";

            play.Logger.error("Problem in saving product information {}", throwable);

            if (throwable.getCause() instanceof SQLIntegrityConstraintViolationException) {

                flashMessage = "The supplied product serial number is already in use";
            } else {

                flashMessage = "A technical error occurred. Please try again later";
            }

            flash("danger", flashMessage);


            return badRequest(views.html.product.create.render(productForm, Ship.find.all()));

        });


    }


    public F.Promise<Result> edit(long id) {

        return Product.find(id).map(product -> {
            if (product == null) {
                flash("danger", "The specified product was not found");

                return redirect(controllers.routes.ProductController.index());
            }

            Form<Product> editProductForm = Form.form(Product.class).fill(product);

            return ok(views.html.product.edit.render(editProductForm, Ship.find.all()));
        });
    }

    public Result delete(long id) {

        return TODO;
    }


    public Result search() {


        Integer iTotalRecords = Inventory.find.findRowCount();


        String GLOBAL_SEARCH_TERM = Optional.ofNullable(request().getQueryString("search[value]")).orElse("");

        String PROD_DESCRIPTION_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[0][search][value]")).orElse("");
        String PROD_SERIAL_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[1][search][value]")).orElse("");
        String SHIP_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[2][search][value]")).orElse("");


        int length = Optional.ofNullable(request().getQueryString("length")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int start = Optional.ofNullable(request().getQueryString("start")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int orderColumn = Optional.ofNullable(request().getQueryString("order[0][column]")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int draw = Optional.ofNullable(request().getQueryString("draw")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        Integer pageSize = length;
        Integer page = start / length;

        /**
         * Get sorting order and column
         */
        String sortBy = "";

        String order = Optional.ofNullable(request().getQueryString("order[0][dir]")).orElse("asc");


        switch (orderColumn) {
            case 0:
                sortBy = "description";
                break;
            case 1:
                sortBy = "serialNumber";
                break;

            case 2:
                sortBy = "ship.description";
                break;

            case 3:
                sortBy = "id";
                break;

            default:
                sortBy = "id";
                break;
        }

        ExpressionList<Product> productExpressionList = Product.find.where();

        if (!PROD_DESCRIPTION_SEARCH_TERM.equals("")) {

            productExpressionList = productExpressionList.ilike("description",  PROD_DESCRIPTION_SEARCH_TERM + "%");
        }

        if (!PROD_SERIAL_SEARCH_TERM.equals("")) {

            productExpressionList = productExpressionList.ilike("serialNumber", PROD_SERIAL_SEARCH_TERM + "%");
        }


        if (!SHIP_SEARCH_TERM.equals("")) {

            productExpressionList = productExpressionList.ilike("ship.description",  SHIP_SEARCH_TERM + "%");
        }


        /*PagedList<Product> productPagedList = Product.find.where().or(

                Expr.or(
                        Expr.ilike("description", "%" + GLOBAL_SEARCH_TERM),

                        Expr.ilike("serialNumber", "%" + GLOBAL_SEARCH_TERM)
                ),
                Expr.ilike("ship.serialNumber", "%" + GLOBAL_SEARCH_TERM)

        ).orderBy(sortBy + " " + order + ", id " + order)
                .findPagedList(page, pageSize);
*/
        PagedList<Product> productPagedList  = productExpressionList.orderBy(sortBy + " " + order + ", id " + order).findPagedList(page, pageSize);

        Integer iTotalDisplayRecords = productPagedList.getTotalRowCount();

        /**
         * Construct the JSON to return
         */
        ObjectNode result = Json.newObject();

        result.put("draw", draw);
        result.put("recordsTotal", iTotalRecords);
        result.put("recordsFiltered", iTotalDisplayRecords);

        ArrayNode an = result.putArray("data");

        for (Product product : productPagedList.getList()) {

            String productDescription = Optional.ofNullable(product.description).orElse("");
            String productSerial = Optional.ofNullable(product.serialNumber).orElse("");
            String ship = Optional.ofNullable(Optional.ofNullable(product.ship).orElse(new Ship("","")).description).orElse("");


            ObjectNode row = Json.newObject();
            row.put("0", productDescription);
            row.put("1", productSerial);
            row.put("2", ship);
            row.put("3", product.id);
            an.add(row);
        }

        return ok(result);
    }


}
