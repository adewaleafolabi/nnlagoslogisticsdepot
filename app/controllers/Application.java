package controllers;


import models.AuditTrails.Login;
import models.AuditTrails.LoginResult;
import models.User;
import models.forms.LoginForm;
import play.*;
import play.cache.Cache;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.*;

import security.AppException;
import security.Hash;
import security.HostMaster;
import security.Secured;
import views.html.*;

public class Application extends Controller {

    public Result index() {
        return ok(views.html.authorization.login.render(Form.form(LoginForm.class)));
    }

    public Result showLogin(){
        return ok(views.html.authorization.login.render(Form.form(LoginForm.class)));
    }

    public Result doLogin(){
        Form<LoginForm> loginForm = Form.form(LoginForm.class).bindFromRequest();

        if(loginForm.hasErrors()){
            play.Logger.error("Form has error -- {}", loginForm.errorsAsJson().toString());

            return badRequest(views.html.authorization.login.render(loginForm));
        }


        session("auth_user_name",loginForm.get().username);

        String destinationUrl = session().get("dest_url");

        play.Logger.debug("From controller ");

        play.Logger.debug("SessionDirect{}",session("dest_url"));


        if(destinationUrl == null){
            return redirect(routes.DashboardController.index());
        }else{
            session("dest_url","");
            return redirect(destinationUrl);
        }
    }

    public Result logout(){
        Logger.debug("Logout initiated");
        User currentUser = new HostMaster().getCurrentUser();

        if( currentUser == null){

            Logger.info("No user currently logged in");

        }else{

            Cache.remove(currentUser.username + "_auth_user");



            session().clear();

            Logger.info("Cache and Session cleared");

            try{
                Login loginTrail=  new Login(LoginResult.LOGOUT_OK,request().remoteAddress(),currentUser);

                play.Logger.info("LoginTrail {}",loginTrail.toString());

                loginTrail.save();

            }catch(Exception e){

                play.Logger.error("failed to save audit trail {}",e);

            }

        }

        Logger.info("user ended session ");

        flash("success", "Logout Successful");


        return  redirect(controllers.routes.Application.index());
    }

    @Security.Authenticated(Secured.class)
    public Result keepAlive(){
        return ok();
    }

}
