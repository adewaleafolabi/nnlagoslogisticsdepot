package controllers;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import play.data.Form;
import play.i18n.Messages;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.Secured;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

/**
 * @author adewaleafolabi
 *
 */
@Security.Authenticated(Secured.class)
public class InventoryController extends Controller {

    public Result search() {


        Map<String, String[]> params = request().queryString();

        String PROD_DESCRIPTION_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[0][search][value]")).orElse("");

        String PROD_SERIAL_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[1][search][value]")).orElse("");

        String LOCATION_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[2][search][value]")).orElse("");

        String SHIP_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[3][search][value]")).orElse("");

        String QUANTITY_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[4][search][value]")).orElse("");


        Integer iTotalRecords = Inventory.find.findRowCount();


        String GLOBAL_SEARCH = Optional.ofNullable(request().getQueryString("search[value]")).orElse("");

        int length = Optional.ofNullable(request().getQueryString("length")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        int start = Optional.ofNullable(request().getQueryString("start")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        int orderColumn = Optional.ofNullable(request().getQueryString("order[0][column]")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        int draw = Optional.ofNullable(request().getQueryString("draw")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        Integer pageSize = length;
        Integer page = start / length;

        /**
         * Get sorting order and column
         */
        String sortBy = "";
        String order = Optional.ofNullable(request().getQueryString("order[0][dir]")).orElse("");

        switch (orderColumn) {
            case 0:
                sortBy = "product.description";
                break;
            case 1:
                sortBy = "product.serialNumber";
                break;
            case 2:
                sortBy = "location";
                break;
            case 3:
                sortBy = "product.ship.description";
                break;
            case 4:
                sortBy = "quantity";
                break;

            case 5:
                sortBy = "id";
                break;

            default:
                sortBy = "id";
                break;
        }

        ExpressionList<Inventory> inventoryExpressionList = Inventory.find.where();

        if (!PROD_DESCRIPTION_SEARCH_TERM.equals("")) {

            inventoryExpressionList = inventoryExpressionList.ilike("product.description",  PROD_DESCRIPTION_SEARCH_TERM + "%");
        }

        if (!PROD_SERIAL_SEARCH_TERM.equals("")) {

            inventoryExpressionList = inventoryExpressionList.ilike("product.serialNumber",  PROD_SERIAL_SEARCH_TERM + "%");
        }

        if (!QUANTITY_SEARCH_TERM.equals("")) {

            inventoryExpressionList = inventoryExpressionList.eq("quantity", QUANTITY_SEARCH_TERM);
        }


        if (!SHIP_SEARCH_TERM.equals("")) {

            inventoryExpressionList = inventoryExpressionList.ilike("product.ship.description",  SHIP_SEARCH_TERM + "%");
        }

        if (!LOCATION_SEARCH_TERM.equals("")) {

            inventoryExpressionList = inventoryExpressionList.ilike("location.coordinates",  LOCATION_SEARCH_TERM + "%");
        }




        PagedList<Inventory> inventoryPagedList = inventoryExpressionList.orderBy(sortBy + " " + order + ", id " + order).findPagedList(page, pageSize);

        Integer iTotalDisplayRecords = inventoryPagedList.getTotalRowCount();

        /**
         * Construct the JSON to return
         */
        ObjectNode result = Json.newObject();

        result.put("draw", draw);
        result.put("recordsTotal", iTotalRecords);
        result.put("recordsFiltered", iTotalDisplayRecords);

        ArrayNode an = result.putArray("data");

        for (Inventory inventory : inventoryPagedList.getList()) {
            Optional<Product> productOptional = Optional.ofNullable(inventory.product);
            Optional<Ship> shipOptional = Optional.ofNullable(inventory.product.ship);
            Optional<Location> locationOptional = Optional.ofNullable(inventory.location);


            String productDescription = productOptional.isPresent() ? productOptional.get().description : "";

            String productSerial = productOptional.isPresent() ? productOptional.get().serialNumber : "";
            String location = locationOptional.isPresent() ? locationOptional.get().coordinates : "";
            String ship = shipOptional.isPresent() ? shipOptional.get().description : "";
            long quantity = Optional.ofNullable(inventory.quantity).isPresent() ? Optional.of(inventory.quantity).get() : 0;


            ObjectNode row = Json.newObject();
            row.put("0", productDescription);
            row.put("1", productSerial);
            row.put("2", location);
            row.put("3", ship);
            row.put("4", quantity);
            row.put("5", inventory.id);
            an.add(row);
        }

        return ok(result);
    }


    public F.Promise<Result> index() {

        return F.Promise.pure(ok(views.html.inventory.dtable.render()));
    }

    public F.Promise<Result> edit(long id) {

        return Inventory.find(id).map(inventory -> {
            if (inventory == null) {
                flash("danger", "The specified inventory was not found");

                return redirect(controllers.routes.InventoryController.index());
            }

            Form<Inventory> editForm = Form.form(Inventory.class).fill(inventory);

            return ok(views.html.inventory.edit.render(editForm, Ship.find.all(), Location.find.all(), Product.find.all()));
        });
    }

    public F.Promise<Result> view(long id) {

        return F.Promise.pure(TODO);
    }

    public F.Promise<Result> delete(long id) {

        return F.Promise.pure(TODO);
    }

    public F.Promise<Result> save() {

        final Form<Inventory> inventoryForm = Form.form(Inventory.class).bindFromRequest();

        if (inventoryForm.hasErrors()) {

            play.Logger.error("Form binding failed with errors -- {}", inventoryForm.errorsAsJson().toString());

            return F.Promise.<Result>pure(badRequest(views.html.inventory.create.render(inventoryForm, Ship.find.all(), Location.find.all(), Product.find.all())));
        }

        Inventory inventory = inventoryForm.get();

        return inventory.asyncSave().map(aVoid -> {
            flash("success", "Inventory information has been saved");

            return redirect(routes.InventoryController.index());
        }).recover(throwable -> {

            String flashMessage = "";

            play.Logger.error("Problem in saving inventory information {}", throwable);

            if (throwable.getCause() instanceof SQLIntegrityConstraintViolationException) {

                flashMessage = "The supplied inventory name is already in use";
            } else {

                flashMessage = "A technical error occurred. Please try again later";
            }

            flash("danger", flashMessage);


            return badRequest(views.html.inventory.create.render(inventoryForm, Ship.find.all(), Location.find.all(), Product.find.all()));

        });
    }

    public F.Promise<Result> update() {


        final Form<Inventory> inventoryForm = Form.form(Inventory.class).bindFromRequest();

        if (inventoryForm.hasErrors()) {

            play.Logger.error("Form binding failed with errors -- {}", inventoryForm.errorsAsJson().toString());

            return F.Promise.<Result>pure(badRequest(views.html.inventory.create.render(inventoryForm, Ship.find.all(), Location.find.all(), Product.find.all())));
        }


        Inventory inventory = inventoryForm.get();
        InventoryHistory inventoryHistory = new InventoryHistory();
        inventoryHistory.entryDate = new Date();
        inventoryHistory.inventory = inventory;
        inventoryHistory.quantity = inventory.quantity;


        Inventory inventoryFromDB = Inventory.find.byId(inventory.id);


        if (inventoryFromDB == null) {
            flash("danger", "Inventory information was not found");
            return F.Promise.pure(redirect(routes.InventoryController.index()));

        }

        return inventory.asyncUpdate().map(aVoid -> {
            flash("success", "Inventory information has been saved");

            return redirect(routes.InventoryController.index());
        }).recover(throwable -> {

            String flashMessage = "";

            play.Logger.error("Problem in updating inventory information {}", throwable);

            if (throwable.getCause() instanceof SQLIntegrityConstraintViolationException) {

                flashMessage = "The supplied inventory name is already in use";
            } else {

                flashMessage = "A technical error occurred. Please try again later";
            }

            flash("danger", flashMessage);


            return badRequest(views.html.inventory.edit.render(inventoryForm, Ship.find.all(), Location.find.all(), Product.find.all()));

        });
    }

    public F.Promise<Result> create() {
        Form<Inventory> inventoryCreationForm = Form.form(Inventory.class);
        return F.Promise.pure(ok(views.html.inventory.create.render(inventoryCreationForm, Ship.find.all(), Location.find.all(), Product.find.all())));
    }
}
