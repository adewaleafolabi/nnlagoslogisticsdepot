package controllers;


import com.avaje.ebean.Expr;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Inventory;
import models.Location;
import models.Product;
import models.Ship;
import play.data.Form;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Map;
import java.util.Optional;

import play.mvc.Security;
import security.Secured;

/**
 * @author adewaleafolabi
 */
@Security.Authenticated(Secured.class)
public class ShipController extends Controller {

    @Security.Authenticated(Secured.class)
    public F.Promise<Result> index() {

        return F.Promise.pure(ok(views.html.ship.list.render()));
    }

    public Result create() {
        Form<Ship> shipCreationForm = Form.form(Ship.class);
        return ok(views.html.ship.create.render(shipCreationForm));
    }


    public F.Promise<Result> update() {

        final Form<Ship> shipForm = Form.form(Ship.class).bindFromRequest();

        if (shipForm.hasErrors()) {

            play.Logger.error("Form binding failed with errors -- {}", shipForm.errorsAsJson().toString());

            return F.Promise.<Result>pure(badRequest(views.html.ship.edit.render(shipForm)));
        }

        Ship ship = shipForm.get();

        return ship.asyncUpdate().map(aVoid -> {
            flash("success", "Ship information has been saved");

            return redirect(routes.ShipController.index());
        }).recover(throwable -> {

            String flashMessage = "";

            play.Logger.error("Problem in saving ship information {}", throwable);

            if (throwable.getCause() instanceof SQLIntegrityConstraintViolationException) {

                flashMessage = "The supplied code or name is already in use";
            } else {

                flashMessage = "A technical error occurred. Please try again later";
            }

            flash("danger", flashMessage);


            return badRequest(views.html.ship.edit.render(shipForm));

        });
    }

    public F.Promise<Result> save() {

        final Form<Ship> shipForm = Form.form(Ship.class).bindFromRequest();

        if (shipForm.hasErrors()) {

            play.Logger.error("Form binding failed with errors -- {}", shipForm.errorsAsJson().toString());

            return F.Promise.<Result>pure(badRequest(views.html.ship.create.render(shipForm)));
        }

        Ship ship = shipForm.get();

        return ship.asyncSave().map(aVoid -> {
            flash("success", "Ship information has been saved");

            return redirect(routes.ShipController.index());
        }).recover(throwable -> {

            String flashMessage = "";

            play.Logger.error("Problem in saving ship information {}", throwable);

            if (throwable.getCause() instanceof SQLIntegrityConstraintViolationException) {

                flashMessage = "The supplied code or name is already in use";
            } else {

                flashMessage = "A technical error occurred. Please try again later";
            }

            flash("danger", flashMessage);


            return badRequest(views.html.ship.create.render(shipForm));

        });


    }


    public F.Promise<Result> edit(long id) {

        return Ship.find(id).map(ship -> {
            if (ship == null) {
                flash("danger", "The specified ship was not found");

                return redirect(controllers.routes.ShipController.index());
            }

            Form<Ship> editShipForm = Form.form(Ship.class).fill(ship);

            return ok(views.html.ship.edit.render(editShipForm));
        });
    }


    public F.Promise<Result> view(long id) {

        int inventoryCount = Inventory.find.where().eq("product.ship.id", id).findRowCount();
        int productCount = Product.find.where().eq("ship.id", id).findRowCount();
        int[] counts = {inventoryCount, productCount};
        return Ship.find(id).map(ship -> {
            if (ship == null) {
                flash("danger", "The specified ship was not found");

                return redirect(controllers.routes.ShipController.index());
            }
            return ok(views.html.ship.view.render(ship, counts));
        });
    }


    public Result delete(long id) {

        return TODO;
    }


    public Result search(long id) {

        Map<String, String[]> params = request().queryString();


        Integer iTotalRecords = Inventory.find.findRowCount();


        String filter = Optional.ofNullable(request().getQueryString("search[value]")).orElse("");

        int length = Optional.ofNullable(request().getQueryString("length")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int start = Optional.ofNullable(request().getQueryString("start")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int orderColumn = Optional.ofNullable(request().getQueryString("order[0][column]")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int draw = Optional.ofNullable(request().getQueryString("draw")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        Integer pageSize = length;
        Integer page = start / length;

        /**
         * Get sorting order and column
         */
        String sortBy = "";
        String order = params.get("order[0][dir]")[0];

        switch (orderColumn) {
            case 0:
                sortBy = "product.description";
                break;
            case 1:
                sortBy = "product.serialNumber";
                break;
            case 2:
                sortBy = "location";
                break;
            case 3:
                sortBy = "product.ship.description";
                break;
            case 4:
                sortBy = "quantity";
                break;

            case 5:
                sortBy = "id";
                break;

            default:
                sortBy = "id";
                break;
        }

        PagedList<Inventory> inventoryPagedList = Inventory.find.where().eq("product.ship.id",id).and(

                Expr.or(
                        Expr.ilike("product.description", "%" + filter + "%"),


                        Expr.eq("quantity", filter)
                ),
                Expr.or(
                        Expr.ilike("product.serialNumber", "%" + filter + "%"),

                        Expr.ilike("product.ship.description", "%" + filter + "%")
                )
        )

                .orderBy(sortBy + " " + order + ", id " + order)
                .findPagedList(page, pageSize);


        Integer iTotalDisplayRecords = inventoryPagedList.getTotalRowCount();

        /**
         * Construct the JSON to return
         */
        ObjectNode result = Json.newObject();

        result.put("draw", draw);
        result.put("recordsTotal", iTotalRecords);
        result.put("recordsFiltered", iTotalDisplayRecords);

        ArrayNode an = result.putArray("data");

        for (Inventory inventory : inventoryPagedList.getList()) {
            Optional<Product> productOptional = Optional.ofNullable(inventory.product);
            Optional<Ship> shipOptional = Optional.ofNullable(inventory.product.ship);
            Optional<Location> locationOptional = Optional.ofNullable(inventory.location);


            String productDescription = productOptional.isPresent() ? productOptional.get().description : "";

            String productSerial = productOptional.isPresent() ? productOptional.get().serialNumber : "";
            String location = locationOptional.isPresent() ? locationOptional.get().coordinates : "";
            String ship = shipOptional.isPresent() ? shipOptional.get().description : "";
            long quantity = Optional.ofNullable(inventory.quantity).isPresent() ? Optional.of(inventory.quantity).get() : 0;


            ObjectNode row = Json.newObject();
            row.put("0", productDescription);
            row.put("1", productSerial);
            row.put("2", location);
            row.put("3", ship);
            row.put("4", quantity);
            row.put("5", inventory.id);
            an.add(row);
        }

        return ok(result);
    }




    public Result dataTable() {


        Integer iTotalRecords = Ship.find.findRowCount();

        String filter = Optional.ofNullable(request().getQueryString("search[value]")).orElse("");

        int length = Optional.ofNullable(request().getQueryString("length")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int start = Optional.ofNullable(request().getQueryString("start")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int orderColumn = Optional.ofNullable(request().getQueryString("order[0][column]")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();


        int draw = Optional.ofNullable(request().getQueryString("draw")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        Integer pageSize = length;
        Integer page = start / length;

        String sortBy = "";

        String order = Optional.ofNullable(request().getQueryString("order[0][dir]")).orElse("asc");


        switch (orderColumn) {
            case 0:
                sortBy = "description";
                break;

            case 1:
                sortBy = "id";
                break;

            default:
                sortBy = "id";
                break;
        }

        PagedList<Ship> shipPagedList = Ship.find.where().ilike("description", "%" + filter + "%")
                .orderBy(sortBy + " " + order + ", id " + order).findPagedList(page, pageSize);

        Integer iTotalDisplayRecords = shipPagedList.getTotalRowCount();

        ObjectNode result = Json.newObject();

        result.put("draw", draw);
        result.put("recordsTotal", iTotalRecords);
        result.put("recordsFiltered", iTotalDisplayRecords);

        ArrayNode an = result.putArray("data");

        shipPagedList.getList().stream().forEach(ship -> {
            String description = Optional.ofNullable(ship.description).orElse("");

            ObjectNode row = Json.newObject();
            row.put("0", description);
            row.put("1", ship.id);
            an.add(row);
        });


        return ok(result);
    }
}
