package controllers;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import org.apache.commons.lang3.RandomStringUtils;
import play.data.Form;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.AppException;
import security.Hash;
import security.RoleSecured;
import security.Secured;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Map;
import java.util.Optional;

/**
 * @author adewaleafolabi
 */
public class UserController extends Controller{

    @Security.Authenticated(Secured.class)
    @RoleSecured(roles = {Role.ADMIN})
    public F.Promise<Result> index() {

        return F.Promise.pure(ok(views.html.user.list.render()));

    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(roles = {Role.ADMIN})
    public Result create() {
        User user = new User();
        user.clearPassword = RandomStringUtils.randomAlphanumeric(8);

        Form<User> userCreationForm = Form.form(User.class).fill(user);

        return ok(views.html.user.create.render(userCreationForm, Role.values()));
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(roles = {Role.ADMIN})
    public F.Promise<Result> save() {
        final Form<User> userForm = Form.form(User.class).bindFromRequest();

        if (userForm.hasErrors()) {

            play.Logger.error("Form binding failed with errors -- {}", userForm.errorsAsJson().toString());

            return F.Promise.<Result>pure(badRequest(views.html.user.create.render(userForm,Role.values())));
        }

        User user = userForm.get();
        user.isEnabled=true;
        user.isValidated=true;

        try{
            user.passwordHash = Hash.createPassword(user.clearPassword);
        }catch (AppException e){

            play.Logger.error("Problem in generating password {}", e);

            flash("danger", "Technical error. Please try again");


            return F.Promise.pure(badRequest(views.html.user.create.render(userForm,Role.values())));


        }

        return user.asyncSave().map(aVoid -> {
            flash("success", "User information has been saved");

            return redirect(routes.UserController.index());
        }).recover(throwable -> {

            String flashMessage = "";

            play.Logger.error("Problem in saving user information {}", throwable);

            if (throwable.getCause() instanceof SQLIntegrityConstraintViolationException) {

                flashMessage = "The supplied user name is already in use";
            } else {

                flashMessage = "A technical error occurred. Please try again later";
            }

            flash("danger", flashMessage);


            return badRequest(views.html.user.create.render(userForm, Role.values()));

        });
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(roles = {Role.ADMIN})
    public F.Promise<Result> update() {
        String message ="";
        final Form<User> userForm = Form.form(User.class).bindFromRequest();

        if (userForm.hasErrors()) {

            play.Logger.error("Form binding failed with errors -- {}", userForm.errorsAsJson().toString());

            return F.Promise.<Result>pure(badRequest(views.html.user.edit.render(userForm,Role.values())));
        }

        User userFromInput = userForm.get();

        User userFromDB = User.find.byId(userFromInput.id);

        if(userFromDB!=null){

            userFromDB.firstName= userFromInput.firstName;
            userFromDB.lastName = userFromInput.lastName;
            userFromDB.middleName = userFromInput.middleName;
            userFromDB.staffNumber = userFromInput.staffNumber;
            userFromDB.role = userFromInput.role;
            if(userFromInput.clearPassword!=null || !userFromInput.clearPassword.isEmpty()){
                try {
                    userFromDB.passwordHash=Hash.createPassword(userFromInput.clearPassword);
                } catch (AppException e) {
                    play.Logger.error("Problem in updating user password {}", e);
                    message= "Problem encountered updating user password";
                    flash("warning", message);


                }
            }

        }


        return userFromDB.asyncUpdate().map(aVoid -> {
            flash("success", "User information has been updated");

            return redirect(routes.UserController.index());
        }).recover(throwable -> {

            String flashMessage = "";

            play.Logger.error("Problem in saving user information {}", throwable);

            if (throwable.getCause() instanceof SQLIntegrityConstraintViolationException) {

                flashMessage = "The supplied user name is already in use";
            } else {

                flashMessage = "A technical error occurred. Please try again later";
            }

            flash("danger", flashMessage);


            return badRequest(views.html.user.edit.render(userForm,Role.values()));

        });
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(roles = {Role.ADMIN})
    public F.Promise<Result> edit(long id) {
        return User.find(id).map(user -> {
            if (user == null) {
                flash("danger", "The specified user was not found");

                return redirect(controllers.routes.UserController.index());
            }

            Form<User> editUserForm = Form.form(User.class).fill(user);

            return ok(views.html.user.edit.render(editUserForm, Role.values()));
        });
    }



    @Security.Authenticated(Secured.class)
    @RoleSecured(roles = {Role.ADMIN})
    public Result delete(long id) {
        return TODO;
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(roles = {Role.ADMIN})
    public Result view(long id) {
        return TODO;
    }


    public Result search() {


        Map<String, String[]> params = request().queryString();

        String USERNAME_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[0][search][value]")).orElse("");

        String ROLE_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[1][search][value]")).orElse("");

        String FIRST_NAME_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[2][search][value]")).orElse("");

        String MIDDLE_NAME_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[3][search][value]")).orElse("");

        String LAST_NAME_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[4][search][value]")).orElse("");
        
        String STAFF_NUMBER_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[5][search][value]")).orElse("");


        Integer iTotalRecords = User.find.where().ne("role", "SUPER_ADMIN").findRowCount();


        String GLOBAL_SEARCH = Optional.ofNullable(request().getQueryString("search[value]")).orElse("");

        int length = Optional.ofNullable(request().getQueryString("length")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        int start = Optional.ofNullable(request().getQueryString("start")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        int orderColumn = Optional.ofNullable(request().getQueryString("order[0][column]")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        int draw = Optional.ofNullable(request().getQueryString("draw")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        Integer pageSize = length;
        Integer page = start / length;

        /**
         * Get sorting order and column
         */
        String sortBy = "";
        String order = Optional.ofNullable(request().getQueryString("order[0][dir]")).orElse("");

        switch (orderColumn) {
            case 0:
                sortBy = "username";
                break;
            case 1:
                sortBy = "role";
                break;
            case 2:
                sortBy = "location";
                break;
            case 3:
                sortBy = "middleName";
                break;
            case 4:
                sortBy = "lastName";
                break;

            case 5:
                sortBy = "staffNumber";
                break;

            default:
                sortBy = "id";
                break;
        }

        ExpressionList<User> userExpressionList = User.find.where().ne("role","SUPER_ADMIN");

        if (!USERNAME_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("username",  USERNAME_SEARCH_TERM + "%");
        }

        if (!ROLE_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("role",  ROLE_SEARCH_TERM + "%");
        }

        if (!LAST_NAME_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("lastName", LAST_NAME_SEARCH_TERM + "%");
        }


        if (!MIDDLE_NAME_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("middleName",  MIDDLE_NAME_SEARCH_TERM + "%");
        }

        if (!FIRST_NAME_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("firstName",  FIRST_NAME_SEARCH_TERM + "%");
        }

        if (!STAFF_NUMBER_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("staffNumber",  STAFF_NUMBER_SEARCH_TERM + "%");
        }




        PagedList<User> userPagedList = userExpressionList.orderBy(sortBy + " " + order + ", id " + order).findPagedList(page, pageSize);

        Integer iTotalDisplayRecords = userPagedList.getTotalRowCount();

        /**
         * Construct the JSON to return
         */
        ObjectNode result = Json.newObject();

        result.put("draw", draw);
        result.put("recordsTotal", iTotalRecords);
        result.put("recordsFiltered", iTotalDisplayRecords);

        ArrayNode an = result.putArray("data");

        for (User user : userPagedList.getList()) {


            String username = Optional.ofNullable(user.username).orElse("");
            String role = Optional.ofNullable(user.role.name()).isPresent()? user.role.name() : "";
            String fistName = Optional.ofNullable(user.firstName).orElse("");
            String middleName = Optional.ofNullable(user.middleName).orElse("");
            String lastName = Optional.ofNullable(user.lastName).orElse("");
            String staffNumber = Optional.ofNullable(user.staffNumber).orElse("");


            ObjectNode row = Json.newObject();
            row.put("0", username);
            row.put("1", role);
            row.put("2", fistName);
            row.put("3", middleName);
            row.put("4", lastName);
            row.put("5", staffNumber);
            row.put("6", user.id);
            an.add(row);
        }

        return ok(result);
    }




    public Result loginAuditSearch() {


        Map<String, String[]> params = request().queryString();

        String USERNAME_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[0][search][value]")).orElse("");

        String ROLE_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[1][search][value]")).orElse("");

        String FIRST_NAME_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[2][search][value]")).orElse("");

        String MIDDLE_NAME_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[3][search][value]")).orElse("");

        String LAST_NAME_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[4][search][value]")).orElse("");

        String STAFF_NUMBER_SEARCH_TERM = Optional.ofNullable(request().getQueryString("columns[5][search][value]")).orElse("");


        Integer iTotalRecords = User.find.where().ne("role", "SUPER_ADMIN").findRowCount();


        String GLOBAL_SEARCH = Optional.ofNullable(request().getQueryString("search[value]")).orElse("");

        int length = Optional.ofNullable(request().getQueryString("length")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        int start = Optional.ofNullable(request().getQueryString("start")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        int orderColumn = Optional.ofNullable(request().getQueryString("order[0][column]")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        int draw = Optional.ofNullable(request().getQueryString("draw")).map(s -> s.matches("\\d+") ? Integer.valueOf(s): 0).get();

        Integer pageSize = length;
        Integer page = start / length;

        /**
         * Get sorting order and column
         */
        String sortBy = "";
        String order = Optional.ofNullable(request().getQueryString("order[0][dir]")).orElse("");

        switch (orderColumn) {
            case 0:
                sortBy = "username";
                break;
            case 1:
                sortBy = "role";
                break;
            case 2:
                sortBy = "location";
                break;
            case 3:
                sortBy = "middleName";
                break;
            case 4:
                sortBy = "lastName";
                break;

            case 5:
                sortBy = "staffNumber";
                break;

            default:
                sortBy = "id";
                break;
        }

        ExpressionList<User> userExpressionList = User.find.where().ne("role","SUPER_ADMIN");

        if (!USERNAME_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("username",  USERNAME_SEARCH_TERM + "%");
        }

        if (!ROLE_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("role",  ROLE_SEARCH_TERM + "%");
        }

        if (!LAST_NAME_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("lastName", LAST_NAME_SEARCH_TERM + "%");
        }


        if (!MIDDLE_NAME_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("middleName",  MIDDLE_NAME_SEARCH_TERM + "%");
        }

        if (!FIRST_NAME_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("firstName",  FIRST_NAME_SEARCH_TERM + "%");
        }

        if (!STAFF_NUMBER_SEARCH_TERM.equals("")) {

            userExpressionList = userExpressionList.ilike("staffNumber",  STAFF_NUMBER_SEARCH_TERM + "%");
        }




        PagedList<User> userPagedList = userExpressionList.orderBy(sortBy + " " + order + ", id " + order).findPagedList(page, pageSize);

        Integer iTotalDisplayRecords = userPagedList.getTotalRowCount();

        /**
         * Construct the JSON to return
         */
        ObjectNode result = Json.newObject();

        result.put("draw", draw);
        result.put("recordsTotal", iTotalRecords);
        result.put("recordsFiltered", iTotalDisplayRecords);

        ArrayNode an = result.putArray("data");

        for (User user : userPagedList.getList()) {


            String username = Optional.ofNullable(user.username).orElse("");
            String role = Optional.ofNullable(user.role.name()).isPresent()? user.role.name() : "";
            String fistName = Optional.ofNullable(user.firstName).orElse("");
            String middleName = Optional.ofNullable(user.middleName).orElse("");
            String lastName = Optional.ofNullable(user.lastName).orElse("");
            String staffNumber = Optional.ofNullable(user.staffNumber).orElse("");


            ObjectNode row = Json.newObject();
            row.put("0", username);
            row.put("1", role);
            row.put("2", fistName);
            row.put("3", middleName);
            row.put("4", lastName);
            row.put("5", staffNumber);
            row.put("6", user.id);
            an.add(row);
        }

        return ok(result);
    }
}
