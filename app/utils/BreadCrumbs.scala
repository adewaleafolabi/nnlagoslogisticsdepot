package utils

/**
 * Created by wale on 11/28/14.
 */
object BreadCrumbs {

  def generate(lastItem:String , menuItems:Map[String,String] ):String ={

    var breadCrumb : String =""

    if(menuItems !=null){

    menuItems.keys.foreach{
      i=>breadCrumb+= "<li class='active'><a href='"+menuItems(i)+"'>"+i+"</a></li>"

    }

    }
    breadCrumb += "<li class='active'>" + lastItem + "</li>"

    return breadCrumb

  }
}
