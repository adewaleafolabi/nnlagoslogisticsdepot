package db.impl;

import db.DBExecutionContext;
import scala.concurrent.ExecutionContext;
import play.libs.Akka;
/**
 * @author adewaleafolabi
 */
public class DBExecutionContextImpl implements DBExecutionContext {
    @Override
    public ExecutionContext getContext() {
        play.Logger.info("Am in DBExecutionContextImpl ");
        return Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }
}
