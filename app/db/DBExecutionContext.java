package db;

import com.google.inject.ImplementedBy;
import db.impl.DBExecutionContextImpl;
import scala.concurrent.ExecutionContext;
/**
 * @author adewaleafolabi
 */
@ImplementedBy(DBExecutionContextImpl.class)
public interface DBExecutionContext {
    public ExecutionContext getContext();
}
