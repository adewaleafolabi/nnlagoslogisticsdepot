
import controllers.Application;
import play.GlobalSettings;
import play.Logger;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

/**
 * Created by wale on 12/18/14.
 */
public class Global extends GlobalSettings {

    public void onStart(Application app) {
        Logger.info("Application has started");
    }

    public void onStop(Application app) {
        Logger.info("Application shutdown...");
    }

    @Override
    public F.Promise<Result> onHandlerNotFound(Http.RequestHeader requestHeader) {

        String requestUrl = requestHeader.path();
        if(requestUrl.endsWith("/")){
            return F.Promise.<Result>pure(Action.redirect(requestUrl.substring(0, requestUrl.length() - 1)));
        }

        return F.Promise.<Result>pure(Results.notFound(views.html.exceptions.notFound.render(requestHeader.uri())));
    }

//    @Override
//    public F.Promise<Result> onError(Http.RequestHeader requestHeader, Throwable throwable) {
//        super.onError(requestHeader, throwable);
//        //return F.Promise.<Result>pure(Results.internalServerError(views.html.exceptions.errorPage.render(throwable)));
//    }


    @Override
    public F.Promise<Result> onBadRequest(Http.RequestHeader requestHeader, String s) {
         super.onBadRequest(requestHeader, s);
        return F.Promise.<Result>pure(Results.notFound(views.html.exceptions.notFound.render(requestHeader.uri())));
    }
}
