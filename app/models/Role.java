package models;

/**
 * @author adewaleafolabi
 */
public enum Role {
    ADMIN,
    SUPERVISOR,
    SUPER_ADMIN,
    UPLOADER
}
