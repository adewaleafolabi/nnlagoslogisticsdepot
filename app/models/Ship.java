package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.inject.Inject;
import db.DBExecutionContext;
import play.libs.Akka;
import play.libs.F;
import scala.concurrent.ExecutionContext;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author adewaleafolabi
 */
@Entity
public class Ship extends Model{
    @Id
    public long id;
    @Column(unique = true)
    public String serialNumber;
    @Column(unique = true)
    public String description;
    @OneToMany
    @JsonIgnore
    public List<Product> products;

    @Inject
    private static DBExecutionContext ctx;

    private ExecutionContext getCtx(){
        return Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }
    public static Finder<Long, Ship> find = new Finder<Long, Ship>(Ship.class);


    public  static F.Promise<List<Ship>> find(){

        return F.Promise.promise(() -> find.all()).recover(throwable -> {
            play.Logger.error("Problem in fetching ships",throwable);
            return new ArrayList<Ship>();
        });
    }


    public  static F.Promise<Ship> find(long id){

        return F.Promise.promise(() -> find.byId(id)).recover(throwable -> {
            play.Logger.error(String.format("Problem in fetching ship with id %s",id),throwable);
            return null;
        });
    }


    public F.Promise<Void> asyncSave(){

        return F.Promise.promise(() -> {
            save();
            return null;
        }, getCtx());

    }

    public F.Promise<Void> asyncUpdate(){

        return F.Promise.promise(() -> {
            update();
            return null;
        }, getCtx());

    }

    public Ship() {
    }

    public Ship(String serialNumber, String description) {
        this.serialNumber = serialNumber;
        this.description = description;
    }
}

