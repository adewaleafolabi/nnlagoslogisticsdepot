package models.vo;

/**
 * @author adewaleafolabi
 */
public class InventoryResponse {
    public String productDescription;
    public String productSerial;
    public String location;
    public String ship;
    public long quantity;

    public InventoryResponse(String productDescription, String productSerial, String location, String ship, long quantity) {
        this.productDescription = productDescription;
        this.productSerial = productSerial;
        this.location = location;
        this.ship = ship;
        this.quantity = quantity;
    }
}
