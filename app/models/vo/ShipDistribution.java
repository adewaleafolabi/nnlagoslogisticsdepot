package models.vo;

/**
 * @author adewaleafolabi
 */
public class ShipDistribution {

    public String name;
    public int amount;

    public ShipDistribution() {
    }

    public ShipDistribution(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }
}
