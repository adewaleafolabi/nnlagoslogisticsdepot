package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.libs.Akka;
import play.libs.F;
import scala.concurrent.ExecutionContext;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author adewaleafolabi
 */
@Entity
@UniqueConstraint(columnNames = {"description","serial_number","ship_id"})
public class Product extends Model{
    @Id
    public long id;
    public String description;
    public String serialNumber;
    @ManyToOne
    public Ship ship;

    @ManyToOne
    @JsonIgnore
    public Location location;

    @OneToMany
    @JsonIgnore
    List<Inventory> inventories;

    
    private ExecutionContext getCtx(){
        return Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }
    public static Model.Finder<Long, Product> find = new Model.Finder<Long, Product>(Product.class);


    public  static F.Promise<List<Product>> find(){

        return F.Promise.promise(() -> find.all()).recover(throwable -> {
            play.Logger.error("Problem in fetching products",throwable);
            return new ArrayList<Product>();
        });
    }

    public  static F.Promise<Product> find(long id){

        return F.Promise.promise(() -> find.byId(id)).recover(throwable -> {
            play.Logger.error(String.format("Problem in fetching product with id %s", id), throwable);
            return null;
        });
    }


    public F.Promise<Void> asyncSave(){

        return F.Promise.promise(() -> {
            save();
            return null;
        }, getCtx());

    }


    public F.Promise<Void> asyncUpdate(){

        return F.Promise.promise(() -> {
            update();
            return null;
        }, getCtx());

    }

    public Product() {
    }

    public Product(String description, String serialNumber, Ship ship) {
        this.description = description;
        this.serialNumber = serialNumber;
        this.ship = ship;
    }
}
