package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import models.AuditTrails.Login;
import play.Logger;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.libs.Akka;
import play.libs.F;
import scala.concurrent.ExecutionContext;
import security.AppException;
import security.Hash;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author adewaleafolabi
 */
@Entity
public class User extends Model {
    @Id
    public long id;

    public String firstName;

    public String middleName;

    public String lastName;


    public String phoneNumber;

    public String staffNumber;

    @Column(unique = true)
    @Formats.NonEmpty
    public String username;

    @Formats.NonEmpty
    public String passwordHash;

    @Transient
    public String clearPassword;

    public boolean isEnabled = false;

    public boolean isValidated;



    @Enumerated(EnumType.STRING)
    public Role role;


    @OneToMany(cascade = CascadeType.ALL)
    @JsonIgnore
    public List<Login> loginAuditTrail = new ArrayList<Login>();

    public static Finder<Long, User> find = new Finder<>(User.class);

    public static User findByUserName(String username){

        return find.where().eq("username",username).findUnique();
    }


    public static User authenticate(String username, String clearPassword) throws AppException {

        User user = find.where().eq("username", username).findUnique();

        if(user == null){

            Logger.debug("user not found");
        }


        if (user != null) {

            Logger.debug("User found");

            if (Hash.checkPassword(clearPassword, user.passwordHash)) {

                return user;
            }

            Logger.error("Login authentication failed for user " + username);

        }

        return null;
    }



    private ExecutionContext getCtx(){
        return Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }


    public  static F.Promise<List<User>> find(){

        return F.Promise.promise(() -> find.where().ne("role",Role.SUPER_ADMIN).findList()).recover(throwable -> {
            play.Logger.error("Problem in fetching users", throwable);
            return new ArrayList<>();
        });
    }


    public  static F.Promise<User> find(long id){

        return F.Promise.promise(() -> find.byId(id)).recover(throwable -> {
            play.Logger.error(String.format("Problem in fetching user with id %s",id),throwable);
            return null;
        });
    }


    public F.Promise<Void> asyncSave(){

        return F.Promise.promise(() -> {
            save();
            return null;
        }, getCtx());

    }

    public F.Promise<Void> asyncUpdate(){

        return F.Promise.promise(() -> {
            update();
            return null;
        }, getCtx());

    }


    public String validate() {

        User user =  User.findByUserName(username);

        if(user !=null && id != user.id) {

            return "The username provided is already registered.";


        }

        return null;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", staffNumber='" + staffNumber + '\'' +
                ", username='" + username + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", isEnabled=" + isEnabled +
                ", isValidated=" + isValidated +
                ", role=" + role +
                ", loginAuditTrail=" + loginAuditTrail +
                "} " + super.toString();
    }
}

