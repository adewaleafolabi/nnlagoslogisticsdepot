package models.forms;

import models.AuditTrails.Login;
import models.AuditTrails.LoginResult;
import models.User;
import play.cache.Cache;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.mvc.Http;
import security.AppException;

import static play.mvc.Http.Context.Implicit.session;

/**
 * @author adewaleafolabi
 */
public class LoginForm {
    @Formats.NonEmpty
    @Constraints.Required
    public String username;
    @Formats.NonEmpty
    @Constraints.Required
    public String password;


    public String validate() {

        User user = null;

        String ipAddress = Http.Context.current().request().remoteAddress();


        try {

            user = User.authenticate(username, password);

        } catch (AppException appEx) {

            play.Logger.error("Error occurred while doing user authentication {}", appEx);

            return "Technical problem occurred. please try again";


        }
        Login loginTrail = null;

        if (user == null) {

            try{
                loginTrail=  new Login(LoginResult.USER_NOT_FOUND,ipAddress,username);
                loginTrail.save();
            }catch(Exception e){

                play.Logger.error("failed to save audit trail {}",e);

                play.Logger.info("LoginTrail {}",loginTrail.toString());
            }

            play.Logger.info("LOGIN ATTEMPT FAILED WITH USERNAME {} ",username);


            return "Invalid username and password combination";

        }

        else if (user.isValidated != true) {

            try{
                loginTrail=  new Login(LoginResult.USER_NOT_VERIFIED,ipAddress,user);

                loginTrail.save();

            }catch(Exception e){

                play.Logger.error("failed to save audit trail {}",e);

                play.Logger.info("LoginTrail {}",loginTrail.toString());
            }

            play.Logger.info("Unvalidated User with ID [{}] tried to login",user.id );

            return "User account is not validated";
        }

        else if (user.isEnabled != true) {

            play.Logger.info("Disabled User with ID [{}] tried to login",user.id );

            try{
                loginTrail=  new Login(LoginResult.USER_NOT_ENABLED,ipAddress,user);

                loginTrail.save();

            }catch(Exception e){

                play.Logger.error("failed to save audit trail {}",e);

                play.Logger.info("LoginTrail {}",loginTrail.toString());
            }

            return "User account is not enabled";
        }

        session().clear();

        session().put("auth_user_name",username);

        session().put("auth_user_role",user.role.name());

        Cache.set(username + "_auth_user", user);

        try{
            loginTrail=  new Login(LoginResult.LOGIN_OK,ipAddress,user);

            loginTrail.save();

        }catch(Exception e){

            play.Logger.error("failed to save audit trail {}",e);

            play.Logger.info("LoginTrail {}",loginTrail.toString());
        }

        play.Logger.info("LOGIN_OK");

        return null;
    }
}
