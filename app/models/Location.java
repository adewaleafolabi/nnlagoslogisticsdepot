package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.libs.Akka;
import play.libs.F;
import scala.concurrent.ExecutionContext;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author adewaleafolabi
 */
@Entity
public class Location extends Model{
    @Id
    public long id;
    @Column(unique = true)
    public String coordinates;

    @OneToMany
    @JsonIgnore
    public List<Product> products;

    @OneToMany
    @JsonIgnore
    public List<Inventory> inventories;

    private ExecutionContext getCtx(){
        return Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }
    public static Model.Finder<Long, Location> find = new Model.Finder<Long, Location>(Location.class);


    public  static F.Promise<List<Location>> find(){

        return F.Promise.promise(() -> find.all()).recover(throwable -> {
            play.Logger.error("Problem in fetching ships",throwable);
            return new ArrayList<Location>();
        });
    }


    public  static F.Promise<Location> find(long id){

        return F.Promise.promise(() -> find.byId(id)).recover(throwable -> {
            play.Logger.error(String.format("Problem in fetching location with id %s",id), throwable);
            return null;
        });
    }


    public F.Promise<Void> asyncSave(){

        return F.Promise.promise(() -> {
            save();
            return null;
        }, getCtx());

    }
public F.Promise<Void> asyncUpdate(){

        return F.Promise.promise(() -> {
            update();
            return null;
        }, getCtx());

    }
}
