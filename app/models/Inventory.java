package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.libs.Akka;
import play.libs.F;
import scala.concurrent.ExecutionContext;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * @author adewaleafolabi
 */
@Entity
public class Inventory extends Model {
    @Id
    public long id;

    @ManyToOne
    public Product product;
    @ManyToOne
    public Location location;

    public long quantity;

    @OneToMany
    @JsonIgnore
    public List<InventoryHistory> inventoryHistory;

    public static Finder<Long, Inventory> find = new Finder<>(Inventory.class);

    private ExecutionContext getCtx() {
        return Akka.system().dispatchers().lookup("akka.db-dispatcher");
    }


    public static F.Promise<List<Inventory>> find() {

        return F.Promise.promise(() -> find.all()).recover(throwable -> {
            play.Logger.error("Problem in fetching inventories", throwable);
            return new ArrayList<>();
        });
    }


    public static F.Promise<Inventory> find(long id) {

        return F.Promise.promise(() -> find.byId(id)).recover(throwable -> {
            play.Logger.error(String.format("Problem in fetching invetory with id %s", id), throwable);
            return null;
        });
    }



    public static F.Promise<List<Inventory>> getPage(int page) {

        return F.Promise.promise(() -> find.findPagedList(page-1 , 15).getList()).recover(throwable -> {
            play.Logger.error(String.format("Problem in fetching inventory list for page  %s", page), throwable);
            return null;
        });
    }


    public F.Promise<Void> asyncSave() {

        return F.Promise.promise(() -> {
            save();
            return null;
        }, getCtx());

    }

    public F.Promise<Void> asyncUpdate() {

        return F.Promise.promise(() -> {
            update();
            return null;
        }, getCtx());

    }

}
