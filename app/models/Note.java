package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * @author adewaleafolabi
 */
@Entity
public class Note {
    @Id
    public long id;

    @Lob
    public String message;


}
