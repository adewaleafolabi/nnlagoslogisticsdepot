package models;

import javax.persistence.*;
import java.util.Date;

/**
 * @author adewaleafolabi
 */
@Entity
public class InventoryHistory {
    @Id
    public long id;
    @ManyToOne
    public Inventory inventory;
    public Date entryDate;
    public long quantity;
    @Enumerated(EnumType.STRING)
    public InventoryHistoryType inventoryHistoryType;
}
