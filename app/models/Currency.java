package models;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author adewaleafolabi
 */
@Entity
public class Currency {
    @Id
    public long id;
    public String code;
    public String description;
}
