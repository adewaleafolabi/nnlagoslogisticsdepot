package models;

/**
 * @author adewaleafolabi
 */
public enum InventoryHistoryType {
    QUANTITY_UPDATED,
    INVENTORY_LOCATION_CHANGED
}
