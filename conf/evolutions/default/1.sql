# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table currency (
  id                        bigint auto_increment not null,
  code                      varchar(255),
  description               varchar(255),
  constraint pk_currency primary key (id))
;

create table inventory (
  id                        bigint auto_increment not null,
  product_id                bigint,
  location_id               bigint,
  quantity                  bigint,
  constraint pk_inventory primary key (id))
;

create table inventory_history (
  id                        bigint auto_increment not null,
  inventory_id              bigint,
  entry_date                datetime(6),
  quantity                  bigint,
  inventory_history_type    varchar(26),
  constraint ck_inventory_history_inventory_history_type check (inventory_history_type in ('QUANTITY_UPDATED','INVENTORY_LOCATION_CHANGED')),
  constraint pk_inventory_history primary key (id))
;

create table location (
  id                        bigint auto_increment not null,
  coordinates               varchar(255),
  constraint uq_location_coordinates unique (coordinates),
  constraint pk_location primary key (id))
;

create table login_audit_trail (
  id                        bigint auto_increment not null,
  event_type                varchar(17),
  event_date                datetime(6),
  ip_address                varchar(255),
  username                  varchar(255),
  user_id                   bigint,
  constraint ck_login_audit_trail_event_type check (event_type in ('LOGIN_OK','USER_NOT_FOUND','USER_NOT_ENABLED','USER_NOT_VERIFIED','LOGOUT_OK')),
  constraint pk_login_audit_trail primary key (id))
;

create table note (
  id                        bigint auto_increment not null,
  message                   longtext,
  constraint pk_note primary key (id))
;

create table product (
  id                        bigint auto_increment not null,
  description               varchar(255),
  serial_number             varchar(255),
  ship_id                   bigint,
  location_id               bigint,
  constraint uq_product_1 unique (description,serial_number,ship_id),
  constraint pk_product primary key (id))
;

create table ship (
  id                        bigint auto_increment not null,
  serial_number             varchar(255),
  description               varchar(255),
  constraint uq_ship_serial_number unique (serial_number),
  constraint uq_ship_description unique (description),
  constraint pk_ship primary key (id))
;

create table user (
  id                        bigint auto_increment not null,
  first_name                varchar(255),
  middle_name               varchar(255),
  last_name                 varchar(255),
  phone_number              varchar(255),
  staff_number              varchar(255),
  username                  varchar(255),
  password_hash             varchar(255),
  is_enabled                tinyint(1) default 0,
  is_validated              tinyint(1) default 0,
  role                      varchar(11),
  constraint ck_user_role check (role in ('ADMIN','SUPERVISOR','SUPER_ADMIN','UPLOADER')),
  constraint uq_user_username unique (username),
  constraint pk_user primary key (id))
;

alter table inventory add constraint fk_inventory_product_1 foreign key (product_id) references product (id) on delete restrict on update restrict;
create index ix_inventory_product_1 on inventory (product_id);
alter table inventory add constraint fk_inventory_location_2 foreign key (location_id) references location (id) on delete restrict on update restrict;
create index ix_inventory_location_2 on inventory (location_id);
alter table inventory_history add constraint fk_inventory_history_inventory_3 foreign key (inventory_id) references inventory (id) on delete restrict on update restrict;
create index ix_inventory_history_inventory_3 on inventory_history (inventory_id);
alter table login_audit_trail add constraint fk_login_audit_trail_user_4 foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_login_audit_trail_user_4 on login_audit_trail (user_id);
alter table product add constraint fk_product_ship_5 foreign key (ship_id) references ship (id) on delete restrict on update restrict;
create index ix_product_ship_5 on product (ship_id);
alter table product add constraint fk_product_location_6 foreign key (location_id) references location (id) on delete restrict on update restrict;
create index ix_product_location_6 on product (location_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table currency;

drop table inventory;

drop table inventory_history;

drop table location;

drop table login_audit_trail;

drop table note;

drop table product;

drop table ship;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

